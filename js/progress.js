//Circular progress bar plugin v1.0
//requires jquery 1.7+ and jquery.rounders.css
//http://custarddoughnuts.co.uk
/*
    Works on latest Chrome, Firefox, Safari and IE9+
    No support for older IE as border radius not supported. I leave you to write your own fallback.
    Note that the plugin uses settimeout calls to do animations in order to dynamically update the percentage counter, so using multiple instances on the same page may result in a noticeable slowdown.

    To initialise on a page:

        $(document).ready(function(){
            $('.progress').rounders({ options });
        });

    Required markup:

        <div class="progress"></div>

    Options:
        - type = circle, disc, hbar (horizontal bar), vbar (vertical bar). Defaults to circle
        - percent = animate to initial starting position as percentage of element, defaults to 0 e.g. a value of 50 will half fill
        - counter = shows a percentage complete indicator inside the circle, 0 or 1. Defaults to on
        - speed = animation speed, milliseconds. Defaults to 1 which is the fastest possible
        - controls, 1 or 0, defaults to 0. Shows some manual controls for playing with the progress. Just for demonstration purposes, not indended for production use as the code is not robust.

    Styling tips:
        - a type of 'disc' adds a class to the 'cover' element to make it seethrough, thus producing a disc effect
        - a border could be added to the to the '.progress' element for circles to make the progress 'inside' the circle
        - circular progress can be 'see through' by turning off the background on '.progress' and setting a background of your choice on the div elements within '.obscure'
        - hbars will fill the available space unless styled differently
        - vbars have a set height and width that can be changed in the stylesheet as required without breaking the plugin
*/
(function(e){var t={autoGrow:function(e,n,r){if(e.percent>n){e.animatefunc(e,-1);setTimeout(function(){t.autoGrow(e,n,r)},r)}else if(e.percent<n){e.animatefunc(e,1);setTimeout(function(){t.autoGrow(e,n,r)},r)}},progressCheck:function(e,t){var n=e.find(".barprogress");if(e.type=="hbar"){var r=parseInt(n.width());var i=r+t;n.css("width",i+"px");if(e.counter){var s=Math.ceil(e.percent/(e.outerWidth()/100))+"%";e.find(".cover").html("<span>Estalvia</span><span>"+s+"</span><span>Menys</span>")}}else if(e.type=="vbar"){var o=parseInt(n.height());var u=o+t;n.css("height",u+"px");if(e.counter){var s=Math.ceil(e.percent/(e.outerHeight()/100))+"%";e.find(".cover").html("Estalvia"+s+"Menys")}}e.percent+=t},rotateCheck:function(e,n){if(e.box<e.boxes.length&&e.box>-1){if(n>0&&e.rotateinner==180&&e.box<e.boxes.length-1){e.rotateinner=0;e.box++}else if(n<0&&e.rotateinner==0&&e.box>0){e.rotateinner=180;e.box--}else{e.rotateinner=Math.min(180,Math.max(0,e.rotateinner+=n));e.percent=Math.min(360,Math.max(0,e.percent+=n))}t.rotateBox(e,e.rotateinner)}},rotateBox:function(e,n){if(e.counter){var r=t.calcPercent(e.percent)+"%";e.find(".cover").html("<span>Estalvia</span><span>"+r+"</span><span>Menys</span>")}e.find(e.boxes[e.box]+"> div").css({transform:"rotate("+n+"deg)","ms-transform":"rotate("+n+"deg)","-moz-tranform":"rotate("+n+"deg)","-webkit-transform":"rotate("+n+"deg)","-o-transform":"rotate("+n+"deg)"})},calcPercent:function(e){return Math.ceil(e/3.6)}};e.fn.rounders=function(n){var r={type:"circle",percent:0,speed:1,counter:1,controls:0};var n=e.extend(r,n);var i;return this.each(function(){obj=e(this);obj.counter=n.counter;obj.type=n.type;if(n.type=="circle"||n.type=="disc"){obj.rotateinner=0;obj.percent=0;obj.boxes=[".right",".left"];obj.box=0;obj.animatefunc=t.rotateCheck;e("<div/>").attr("class","obscure right").appendTo(obj);e("<div/>").attr("class","obscure left").appendTo(obj);e("<div/>").appendTo(obj.find(".obscure.right"));e("<div/>").appendTo(obj.find(".obscure.left"));e("<div/>").attr("class","cover").appendTo(obj);obj.addClass("circle");if(n.type=="disc"){obj.addClass("disc")}if(n.percent>0&&n.percent<=100){n.percent=Math.ceil(n.percent*3.6);t.autoGrow(obj,n.percent,n.speed)}}else if(n.type=="hbar"||n.type=="vbar"){obj.percent=0;obj.addClass("bar");obj.type=n.type;obj.animatefunc=t.progressCheck;e("<div/>").attr("class","barprogress").appendTo(obj);e("<div/>").attr("class","cover").appendTo(obj);if(n.type=="hbar"){obj.addClass("hbar")}else if(n.type=="vbar"){obj.addClass("vbar")}if(n.percent>0&&n.percent<=100){if(n.type=="hbar"){var r=Math.ceil(n.percent*(obj.outerWidth()/100))}else if(n.type=="vbar"){var r=Math.ceil(n.percent*(obj.outerHeight()/100))}t.autoGrow(obj,r,n.speed)}}if(n.controls){$controls=e("<div/>").addClass("controls");e("<input/>").addClass("c_more").attr("type","button").val("+").appendTo($controls);e("<input/>").addClass("c_less").attr("type","button").val("-").appendTo($controls);e("<input/>").addClass("c_manual").attr("type","text").appendTo($controls);e("<input/>").addClass("c_go").attr("type","button").val("Go").appendTo($controls);$controls.appendTo(obj);i=obj;obj.find(".c_more").on("click",function(){i.animatefunc(i,1)});obj.find(".c_less").on("click",function(){i.animatefunc(i,-1)});e(this).find(".c_go").on("click",function(){var r=e(this).closest(".progress");var s=parseInt(r.find(".c_manual").val());if(s>=0&&s<=100){if(i.type=="circle"||i.type=="disc"){s=Math.ceil(s*3.6)}else if(n.type=="hbar"){s=Math.ceil(s*(i.outerWidth()/100))}else if(n.type=="vbar"){s=Math.ceil(s*(i.outerHeight()/100))}t.autoGrow(i,s,n.speed)}})}})}})(jQuery)