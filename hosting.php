<?php include './php/header/variables.php'; ?>    
<?php include './php/languages/cat.php'; ?>    
<?php
if (!isset($_COOKIE['lenguaje'])){
     $sitelangA = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if (preg_match('/es/i',$sitelangA)) {
setcookie('lenguaje', "es");
echo '<meta http-equiv="refresh" content="0">';
} 
elseif (preg_match('/ca/i',$sitelangA)) 
{
setcookie('lenguaje', "cat");
echo '<meta http-equiv="refresh" content="0">';
}
elseif (preg_match('/en/i',$sitelangA)) {
setcookie('lenguaje', "en");
echo '<meta http-equiv="refresh" content="0">';
}
}

if ($_COOKIE['lenguaje'] == "es") {
	include './php/languages/es.php';
} elseif ($_COOKIE['lenguaje'] == "cat") {
	include './php/languages/cat.php';
} elseif ($_COOKIE['lenguaje'] == "en") {
	include './php/languages/en.php';
}	
?>
<!DOCTYPE html>
<html lang="es-ES">
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $n20; ?> - <?php echo $n21; ?></title>
	<?php echo $csslinks; ?>
  </head>
<?php include './php/header/headermenu2.php'; ?>  
<?php echo cookieslaw(); ?>
    <div class="wrapper" id="subheader">
    	<div class="container">
        	<h1><i class="fa fa-bullhorn"></i>Hosting</h1>
        </div>
    </div>
    <div class="wrapper" id="featuredplans">
    	<div class="container">
        	<div class="content-heading white">
    			<h2><?php echo $n68; ?></h2>
                <small><?php echo $n69; ?></small>
                <span></span>
            </div>
            <div class="row lesspad">
                    <div class="col-md-3 col-sm-6">
                        <div class="plan notfeatured">
                        	<span class="price">6<?php echo $n22; ?></span>
                            <h3><?php echo $n5; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                                <li><span class="fa fa-download"></span>5 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span>5 <?php echo $n27; ?></li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span>2 <?php echo $n28; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span>1 <?php echo $n29; ?></li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="/shop/start?t=1"><?php echo $n31; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan red featured">
                        	<span class="price">10<?php echo $n22; ?></span>
                            <h3><?php echo $n6; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                                <li><span class="fa fa-download"></span>10 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span>15 <?php echo $n27; ?></li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span>8 <?php echo $n28; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span>3 <?php echo $n29; ?></li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="/shop/start?t=2"><?php echo $n31; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan purple notfeatured">
                        	<span class="price">15<?php echo $n22; ?></span>
                            <h3><?php echo $n7; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                                <li><span class="fa fa-download"></span>20 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span><?php echo $n27; ?> Ilimitats</li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span>15 <?php echo $n28; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span>6 <?php echo $n29; ?></li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="/shop/start?t=3"><?php echo $n31; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan green notfeatured">
                        	<span class="price">20<?php echo $n22; ?></span>
                            <h3><?php echo $n8; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                               <li><span class="fa fa-download"></span>30 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span><?php echo $n27; ?> Ilimitats</li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span><?php echo $n99; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span><?php echo $n29; ?> Ilimitades</li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="/shop/start?t=4"><?php echo $n31; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    </div>
		</div>
    </div>
    
    <div class="wrapper" id="optionbar">
    	<div class="container">
        	<ul>
            	<li ><a href="#" class="hosting-toggle" data-toggle="hostingfeatures"><i class="fa fa-cog"></i><?php echo $n70; ?></a></li><li class="active"><a href="#" class="hosting-toggle" data-toggle="hostingcompare"><i class="fa fa-table"></i><?php echo $n71; ?></a></li>
            </ul>
        </div>
    </div>
    
    <div class="wrapper" id="maincontent">
    	<div class="container">
        	 <div class="row compareplans dropdown-box active" id="hostingcompare">
             <div class="content-heading">
    			<h2><?php echo $n71; ?></h2>
                <small><?php echo $n72; ?></small>
                <span></span>
            </div>
             	<ul>
                	<li class="row-fluid">
                    	<ul class="compare-heading">
                        	<li class="col-sm-2"></li>
                            <li class="col-xs-2 blue"><?php echo $n98; ?><span>8<?php echo $n22; ?></span></li>
                            <li class="col-xs-2 red featured"><?php echo $n5; ?><br><br><span>6<?php echo $n22; ?></span></li>
                            <li class="col-xs-2 purple"><?php echo $n6; ?><br><br><span>10<?php echo $n22; ?></span></li>
                            <li class="col-xs-2 green"><?php echo $n7; ?><br><br><span>15<?php echo $n22; ?></span></li>
                            <li class="col-xs-2"><?php echo $n8; ?><br><br><span>20<?php echo $n22; ?></span></li>
                        </ul>
                    </li>
                    <li class="compare-separator"></li>
                    <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n73; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n74; ?>"></span></li>
                            <li class="col-xs-2">5 GB</li>
                            <li class="col-xs-2 featured">5 GB</li>
                            <li class="col-xs-2">10 GB</li>
                            <li class="col-xs-2">20 GB</li>
                            <li class="col-xs-2">30 GB</li>
                        </ul>
                    </li>
                     <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n75; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n77; ?>"></span></li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                            <li class="col-xs-2 featured"><?php echo $n96; ?></li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                        </ul>
                    </li>
                     <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n76; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n78; ?>"></span></li>
                            <li class="col-xs-2">100%</li>
                            <li class="col-xs-2 featured">100%</li>
                            <li class="col-xs-2">100%</li>
                            <li class="col-xs-2">100%</li>
                            <li class="col-xs-2">100%</li>
                        </ul>
                    </li>
                     <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n79; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n80; ?>"></span></li>
                            <li class="col-xs-2">1</li>
                            <li class="col-xs-2 featured">1</li>
                            <li class="col-xs-2">1</li>
                            <li class="col-xs-2">1</li>
                            <li class="col-xs-2">1</li>
                        </ul>
                    </li>
                     <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n27; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n81; ?>"></span></li>
                            <li class="col-xs-2">5</li>
                            <li class="col-xs-2 featured">5</li>
                            <li class="col-xs-2">15</li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                        </ul>
                    </li>
                     <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n82; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n83; ?>"></span></li>
                            <li class="col-xs-2"><?php echo $n97; ?></li>
                            <li class="col-xs-2 featured"><?php echo $n97; ?></li>
                            <li class="col-xs-2"><?php echo $n97; ?></li>
                            <li class="col-xs-2"><?php echo $n97; ?></li>
                            <li class="col-xs-2"><?php echo $n97; ?></li>
                        </ul>
                    </li>
                    
                    <li class="compare-separator"><?php echo $n84; ?></li>
                    <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n85; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n86; ?>"></span></li>
                            <li class="col-xs-2">1</li>
                            <li class="col-xs-2 featured">1</li>
                            <li class="col-xs-2">4</li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                        </ul>
                    </li>
                                        <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n89; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n91; ?>"></span></li>
                            <li class="col-xs-2">1</li>
                            <li class="col-xs-2 featured">1</li>
                            <li class="col-xs-2">3</li>
                            <li class="col-xs-2">6</li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                        </ul>
                    </li>
                                        <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n90; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n91; ?>"></span></li>
                            <li class="col-xs-2">1</li>
                            <li class="col-xs-2 featured">1</li>
                            <li class="col-xs-2">3</li>
                            <li class="col-xs-2">6</li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                        </ul>
                    </li>
                    <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n93; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n92; ?>"></span></li>
                            <li class="col-xs-2">1</li>
                            <li class="col-xs-2 featured">2</li>
                            <li class="col-xs-2">8</li>
                            <li class="col-xs-2">15</li>
                            <li class="col-xs-2"><?php echo $n96; ?></li>
                        </ul>
                    </li>
                     <li class="row-fluid compare-data">
                    	<ul>
                        	<li class="col-sm-2"><?php echo $n87; ?> <span class="help-toggle fa fa-question-circle" data-toggle="popover" data-placement="top" data-content="<?php echo $n88; ?>"></span></li>
                            <li class="col-xs-2"><i class="glyphicon glyphicon-ok"></i></li>
                            <li class="col-xs-2 featured"><i class="glyphicon glyphicon-ok"></i></li>
                            <li class="col-xs-2"><i class="glyphicon glyphicon-ok"></i></li>
                            <li class="col-xs-2"><i class="glyphicon glyphicon-ok"></i></li>
                            <li class="col-xs-2"><i class="glyphicon glyphicon-ok"></i></li>
                        </ul>
                    </li>
                    <li class="compare-separator"></li>
                    <li class="row-fluid compare-order">
                    	<ul>
                        	<li class="col-sm-2"></li>
                            <li class="col-xs-2"><a href="/shop/start?t=5" class="btn btn-lg btn-blue"><?php echo $n31; ?></a></li>
                            <li class="col-xs-2"><a href="/shop/start?t=1" class="btn btn-lg btn-red"><?php echo $n31; ?></a></li>
                            <li class="col-xs-2"><a href="/shop/start?t=2" class="btn btn-lg btn-purple"><?php echo $n31; ?></a></li>
                            <li class="col-xs-2"><a href="/shop/start?t=3" class="btn btn-lg btn-green"><?php echo $n31; ?></a></li>
                            <li class="col-xs-2"><a href="/shop/start?t=4" class="btn btn-lg btn-blue"><?php echo $n31; ?></a></li>
                        </ul>
                    </li>
                </ul>
                
              <div class="help-box">
            	<h2><i class="fa fa-comments"></i> <?php echo $n94; ?> <a class="btn btn-lg btn-green" href="support"><?php echo $n95; ?></a></h2>
            </div>
             </div>
			 <div class="mainfeatures dropdown-box" id="hostingfeatures">
               <div class="content-heading padtop30">
    			<h2><?php echo $n50; ?></h2>
                <small><?php echo $n51; ?></small>
                <span class="divider"></span>
            </div>
            <div class="row">
            	<div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-briefcase"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n52; ?></h3>
                            <p><?php echo $n53; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n54; ?></h3>
                            <p><?php echo $n55; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-cog"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n56; ?></h3>
                            <p><?php echo $n57; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-file"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n58; ?></h3>
                            <p><?php echo $n59; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-headphones"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n60; ?></h3>
                            <p><?php echo $n61; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-euro"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n62; ?></h3>
                            <p><?php echo $n63; ?></p>
                         </div>
                    </div>
                </div>
               </div>
            </div>
        </div>
    </div>
    <div class="wrapper" id="darkerfooter">
<?php include './php/footer/footer.php' ; ?>

<?php echo $jslinks; ?>
  </body>
</html>