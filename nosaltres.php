<?php include './php/header/variables.php'; ?>    
<?php include './php/languages/cat.php'; ?>    
<?php
if (!isset($_COOKIE['lenguaje'])){
     $sitelangA = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if (preg_match('/es/i',$sitelangA)) {
setcookie('lenguaje', "es");
echo '<meta http-equiv="refresh" content="0">';
} 
elseif (preg_match('/ca/i',$sitelangA)) 
{
setcookie('lenguaje', "cat");
echo '<meta http-equiv="refresh" content="0">';
}
elseif (preg_match('/en/i',$sitelangA)) {
setcookie('lenguaje', "en");
echo '<meta http-equiv="refresh" content="0">';
}
}

if ($_COOKIE['lenguaje'] == "es") {
	include './php/languages/es.php';
} elseif ($_COOKIE['lenguaje'] == "cat") {
	include './php/languages/cat.php';
} elseif ($_COOKIE['lenguaje'] == "en") {
	include './php/languages/en.php';
}	
?>
<!DOCTYPE html>
<html lang="es-ES">
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $n20; ?> - <?php echo $n21; ?></title>
	<?php echo $csslinks; ?>
  </head>
  <?php include './php/header/headermenu2.php'; ?>  
  <?php echo cookieslaw(); ?>
    <div class="wrapper green-content">
    	<div class="container">
        	<div class="content-heading white">
    			<h2><?php echo $n100; ?></h2>
                <small><?php echo $n101; ?></small>
                <span></span>
            </div>
            <div class="row featuredposts">
				<div class="col-sm-4">
                	<div class="post">
                    	<a href="https://twitter.com/torralbo_" target="_blank">torralbo_</a>
                        <h2>David Torralbo</h2>
                        <p><?php echo $n105; ?></p>
                        <div class="testimonial-image"><img alt="David Torralbo" src="images/nosaltres/david.jpg" ></div>
                        <small>@torralbo_</small>
                    </div>
                </div>
				<div class="col-sm-4">
                	<div class="post">
                    	<a href="https://twitter.com/Denisedahi" target="_blank">Denisedahi</a>
                        <h2>Den&iacute;s Serra</h2>
                        <p><?php echo $n106; ?></p>
                        <div class="testimonial-image"><img alt="Denís Serra" src="images/nosaltres/denis.jpeg" ></div>
                        <small>@Denisedahi</small>
                    </div>
                </div>
                <div class="col-sm-4">
                	<div class="post">
                    	<a href="https://twitter.com/aviicii002" target="_blank">aviicii002</a>
                        <h2>Marius Alexandru</h2>
                        <p><?php echo $n107; ?></p>
                        <div class="testimonial-image"><img alt="Marius Alexandru" src="images/nosaltres/marius.jpg" ></div>
                        <small>@aviicii002</small>
                    </div>
                </div>
            </div> 
		</div>
    </div>
    
    <div class="wrapper green" id="optionbar">
    	<div class="container">
        	<ul>
            	<li class="active"><a href="#" class="hosting-toggle" data-toggle="hostingguarantee"><i class="fa fa-thumbs-up"></i>Treballa amb nosaltres</a></li>
            </ul>
        </div>
    </div>
    
    <div class="wrapper" id="maincontent">
    	<div class="container">
         <div class="dropdown-box active" id="hostingguarantee">
            	<div class="col-sm-3">
                	<img src="images/nosaltres/you.jpg" alt="alt"><br><br><br>
                </div>
                <div class="col-sm-9">
                	<h2><?php echo $n102; ?></h2>
                    <p><?php echo $n104; ?></p>
                    <a class="btn btn-lg btn-red no-margin-left" href="#"><?php echo $n103; ?></a>
                </div>
            </div>
            	<div class="featuredposts dropdown-box active" id="clientbox">
                <div class="suboptions">
                    
                </div>
            
        </div>
    </div>
    </div>
    <div class="wrapper" id="darkerfooter">
<?php include './php/footer/footer.php' ; ?>
<?php echo $jslinks; ?>
  </body>
</html>