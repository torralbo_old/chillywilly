            <?php
			if (isset($_GET['lang'])) {
			if ($_GET['lang'] == "cat") {
	include './php/languages/cat.php';
setcookie('lenguaje', "cat");
			}
			elseif ($_GET['lang'] == "es") {
include './php/languages/es.php';
setcookie('lenguaje', "es");
			}
			elseif ($_GET['lang'] == "en") {
include './php/languages/en.php';
setcookie('lenguaje', "en");
		} 
			elseif ($_GET['lang'] != "en" or $_GET['lang'] != "es" or $_GET['lang'] != "cat" ) {
				if (!isset($_COOKIE['lenguaje'])){
     $sitelangA = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if (preg_match('/es/i',$sitelangA)) {
setcookie('lenguaje', "es");
} 
elseif (preg_match('/ca/i',$sitelangA)) 
{
setcookie('lenguaje', "cat");
}
elseif (preg_match('/en/i',$sitelangA)) {
setcookie('lenguaje', "en");
}
}

if ($_COOKIE['lenguaje'] == "es") {
	include './php/languages/es.php';
} elseif ($_COOKIE['lenguaje'] == "cat") {
	include './php/languages/cat.php';
} elseif ($_COOKIE['lenguaje'] == "en") {
	include './php/languages/en.php';
}
			}}
			?>
  <body id="home">
  	<div class="wrapper" id="topbar">
    	<div class="container">
        	<ul>
            	<li><a href="#" class="dropdown-toggle dropdown-bottom" data-toggle="supportbox"><span class="glyphicon glyphicon-headphones"></span> <?php echo $n11; ?></a>
                	<div class="hiddenbox" id="supportbox">
						<div class="livesupport"><a href="#"><img alt="alt" src="images/icons/livesupport.png"><?php echo $n17; ?></a></div>
                        <div class="row-fluid">
                        	<div class="col-xs-6"><a href="/support"><span class="glyphicon glyphicon-list-alt"></span> <?php echo $n18; ?></a></div>
                            <div class="col-xs-6"><a href="mailto:mailman@chillywilly.cat"><span class="glyphicon glyphicon-envelope"></span> <?php echo $n19; ?></a></div>
                        </div>
                    </div>
                </li>
                <li><a href="http://gnupanel.<?php echo $domini; ?>/users/"><span class="glyphicon glyphicon-user"></span> <?php echo $n16; ?></a></li>
            </ul>
        </div>
    </div>
    
    
    <div class="wrapper" id="topcontent">
    	<div class="container">
        
        	<div id="header">
                <div class="logo"><a href="/?<?php echo random() ?>"><?php echo $n9; ?></a></div>
             	<div class="phonemenu"><span class="glyphicon glyphicon-align-justify"></span></div>
                <ul>
                	    <li class="dropdown-stop"><a href="hosting" class="dropdown-bottom"><?php echo $n237; ?></a>
                    	<ul>
                            <li><a href="?lang=cat"><?php echo $n13; ?></a></li>
                            <li><a href="?lang=es"><?php echo $n14; ?></a></li>
                            <li><a href="?lang=en"><?php echo $n15; ?></a></li>
                        </ul>
                    </li>
                    <li><a href="support?<?php echo random() ?>"><?php echo $n11; ?></a></li>
                    <li><a href="http://blog.<?php echo $domini; ?>"><?php echo $n10; ?></a></li>
                    <li><a href="nosaltres?<?php echo random() ?>"><?php echo $n45; ?></a></li>
                    <li><a href="hosting?<?php echo random() ?>"><?php echo $n1; ?></a></li>
                    <li class="active"><a href="?<?php echo random() ?>"><?php echo $n2; ?></a></li>

                </ul>
            </div>
