<?php include './php/header/variables.php'; ?>    
<?php
if (!isset($_COOKIE['lenguaje'])){
     $sitelangA = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if (preg_match('/es/i',$sitelangA)) {
setcookie('lenguaje', "es");
echo '<meta http-equiv="refresh" content="0">';
} 
elseif (preg_match('/ca/i',$sitelangA)) 
{
setcookie('lenguaje', "cat");
echo '<meta http-equiv="refresh" content="0">';
}
elseif (preg_match('/en/i',$sitelangA)) {
setcookie('lenguaje', "en");
echo '<meta http-equiv="refresh" content="0">';
}
}

if ($_COOKIE['lenguaje'] == "es") {
	include './php/languages/es.php';
} elseif ($_COOKIE['lenguaje'] == "cat") {
	include './php/languages/cat.php';
} elseif ($_COOKIE['lenguaje'] == "en") {
	include './php/languages/en.php';
}	
?>
<!DOCTYPE html>
<html lang="es-ES">
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $n20; ?> - <?php echo $n21; ?></title>
	<?php echo $csslinks; ?>
  </head>
<?php include './php/header/headermenu2.php'; ?> 
<?php echo cookieslaw(); ?>
    <div class="wrapper purple-content">
    	<div class="container">
        	<div class="content-heading white">
    			<h2><?php echo $n110; ?></h2>
                <small><?php echo $n111; ?></small>
            </div>
		</div>
    </div>
    
    <div class="wrapper purple" id="optionbar">
    	<div class="container">
        	<ul>
            	<li class="active"><a href="#" class="hosting-toggle" data-toggle="contactbox"><i class="fa fa-envelope"></i><?php echo $n108; ?></a></li>
            </ul>
        </div>
    </div>
    
    <div class="wrapper" id="maincontent">
    	<div class="container">
         
                   
            <div class="dropdown-box active" id="contactbox">
                <div class="content-heading">
                    <h2><?php echo $n112; ?></h2>
                    <small><?php echo $n113; ?></small>
                    <span></span>
                </div>
                <?php echo formularicontacte(); ?>
                <div class="col-sm-4"><h3><i class="fa fa-twitter"></i> Twitter</h3>
                    <p><a href="https://twitter.com/CWHostings" target="_blank">@CWHostings</a></p>
                </div>
                <br><br><br><br>
                <div class="col-sm-4"><h3><i class="fa fa-question-circle"></i> FAQ</h3>
                    <p><a href="http://blog.chillywilly.cat/category/faq" target="_blank"><?php echo $n236; ?></a></p>
                </div>
            </div>
            
             <div class="dropdown-box" id="faqbox">
              	<div class="content-heading">
                    <h2>FAQs</h2>
                    <small>Things People are Asking</small>
                    <span></span>
                </div>
            	<ul>
                	<li>
                    	<h3><a href="#"><i class="fa fa-question-circle"></i> How Do I Set Up My Hosting?</a></h3>
                    	<div class="answer active"><p>Maecenas quis tincidunt sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel tempus metus, sed placerat lacus. Suspendisse tristique efficitur nisi at ornare. Phasellus a urna sem. Phasellus non maximus neque, et convallis est..</p></div>
                    </li>
                    <li>
                    	<h3><a href="#"><i class="fa fa-question-circle"></i> Another Question Which I Want Answered?</a></h3>
                    	<div class="answer"><p>Maecenas quis tincidunt sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel tempus metus, sed placerat lacus. Suspendisse tristique efficitur nisi at ornare. Phasellus a urna sem. Phasellus non maximus neque, et convallis est..</p></div>
                    </li>
                     <li>
                    	<h3><a href="#"><i class="fa fa-question-circle"></i> Another Question Which I Want Answered?</a></h3>
                    	<div class="answer"><p>Maecenas quis tincidunt sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel tempus metus, sed placerat lacus. Suspendisse tristique efficitur nisi at ornare. Phasellus a urna sem. Phasellus non maximus neque, et convallis est..</p></div>
                    </li>
                    <li>
                    	<h3><a href="#"><i class="fa fa-question-circle"></i> Another Question Which I Want Answered?</a></h3>
                    	<div class="answer"><p>Maecenas quis tincidunt sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel tempus metus, sed placerat lacus. Suspendisse tristique efficitur nisi at ornare. Phasellus a urna sem. Phasellus non maximus neque, et convallis est..</p></div>
                    </li>
                    <li>
                    	<h3><a href="#"><i class="fa fa-question-circle"></i> Another Question Which I Want Answered?</a></h3>
                    	<div class="answer"><p>Maecenas quis tincidunt sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel tempus metus, sed placerat lacus. Suspendisse tristique efficitur nisi at ornare. Phasellus a urna sem. Phasellus non maximus neque, et convallis est..</p></div>
                    </li>
                    <li>
                    	<h3><a href="#"><i class="fa fa-question-circle"></i> Another Question Which I Want Answered?</a></h3>
                    	<div class="answer"><p>Maecenas quis tincidunt sem. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nullam vel tempus metus, sed placerat lacus. Suspendisse tristique efficitur nisi at ornare. Phasellus a urna sem. Phasellus non maximus neque, et convallis est..</p></div>
                    </li>    
                 </ul>
            </div>
            
        </div>
    </div>
	   <div class="wrapper" id="darkerfooter">
<?php include './php/footer/footer.php' ; ?>

<?php echo $jslinks; ?>
  </body>
</html>