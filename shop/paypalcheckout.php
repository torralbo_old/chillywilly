<?php
// CONFIG: Enable debug mode. This means we'll log requests into 'ipn.log' in the same directory.
// Especially useful if you encounter network errors or other intermittent problems with IPN (validation).
// Set this to 0 once you go live or don't require logging.
define("DEBUG", 0);
// Set to 0 once you're ready to go live
define("USE_SANDBOX", 0);
define("LOG_FILE", "./ipn.log");
// Read POST data
// reading posted data directly from $_POST causes serialization
// issues with array data in POST. Reading raw POST data from input stream instead.
$raw_post_data = file_get_contents('php://input');
$raw_post_array = explode('&', $raw_post_data);
$myPost = array();
foreach ($raw_post_array as $keyval) {
	$keyval = explode ('=', $keyval);
	if (count($keyval) == 2)
		$myPost[$keyval[0]] = urldecode($keyval[1]);
}
// read the post from PayPal system and add 'cmd'
$req = 'cmd=_notify-validate';
if(function_exists('get_magic_quotes_gpc')) {
	$get_magic_quotes_exists = true;
}
foreach ($myPost as $key => $value) {
	if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
		$value = urlencode(stripslashes($value));
	} else {
		$value = urlencode($value);
	}
	$req .= "&$key=$value";
}
// Post IPN data back to PayPal to validate the IPN data is genuine
// Without this step anyone can fake IPN data
if(USE_SANDBOX == true) {
	$paypal_url = "https://www.sandbox.paypal.com/cgi-bin/webscr";
} else {
	$paypal_url = "https://www.paypal.com/cgi-bin/webscr";
}
$ch = curl_init($paypal_url);
if ($ch == FALSE) {
	return FALSE;
}
curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
if(DEBUG == true) {
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLINFO_HEADER_OUT, 1);
}
// CONFIG: Optional proxy configuration
//curl_setopt($ch, CURLOPT_PROXY, $proxy);
//curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, 1);
// Set TCP timeout to 30 seconds
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close'));
// CONFIG: Please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set the directory path
// of the certificate as shown below. Ensure the file is readable by the webserver.
// This is mandatory for some environments.
//$cert = __DIR__ . "./cacert.pem";
//curl_setopt($ch, CURLOPT_CAINFO, $cert);
$res = curl_exec($ch);
if (curl_errno($ch) != 0) // cURL error
	{
	if(DEBUG == true) {	
		error_log(date('[Y-m-d H:i e] '). "Can't connect to PayPal to validate IPN message: " . curl_error($ch) . PHP_EOL, 3, LOG_FILE);
	}
	curl_close($ch);
	exit;
} else {
		// Log the entire HTTP response if debug is switched on.
		if(DEBUG == true) {
			error_log(date('[Y-m-d H:i e] '). "HTTP request of validation request:". curl_getinfo($ch, CURLINFO_HEADER_OUT) ." for IPN payload: $req" . PHP_EOL, 3, LOG_FILE);
			error_log(date('[Y-m-d H:i e] '). "HTTP response of validation request: $res" . PHP_EOL, 3, LOG_FILE);
		}
		curl_close($ch);
}
// Inspect IPN validation result and act accordingly
// Split response headers and payload, a better way for strcmp
$tokens = explode("\r\n\r\n", trim($res));
$res = trim(end($tokens));
if (strcmp ($res, "VERIFIED") == 0) {
	// check whether the payment_status is Completed
	// check that txn_id has not been previously processed
	// check that receiver_email is your PayPal email
	// check that payment_amount/payment_currency are correct
	// process payment and mark item as paid.
	// assign posted variables to local variables
    $item_name = $_GET['item_name'];
    $item_number = $_GET['item_number'];
    $payment_status = $_GET['payment_status'];
    $payment_amount = $_GET['mc_gross'];
    $payment_currency = $_GET['mc_currency'];
    $txn_id = $_GET['txn_id'];
    $receiver_email = $_GET['receiver_email'];
    $payer_email = $_GET['payer_email'];
    $custom = $_GET['custom'];
	$residence_country = $_GET['residence_country'];
    $payment_date = $_GET['payment_date'];
	$option_selection2 = $_GET['option_selection2'];
	$option_selection1 = $_GET['option_selection1'];
	$creditos=substr($payment_amount,1);
	// REGISTRE IPN PAYPAL
	 include '../php/languages/cat.php'; 
	  include_once 'connect.php';
	  include 'functions.php';
	  $extractdades = mysql_query("SELECT * FROM `dadesowners` WHERE `domini` = '$domini'");
$arraydades = mysql_fetch_array($extractdades);
$extractdadesdomini = mysql_query("SELECT * FROM `dominishosting` WHERE `domini` = '$dominicheck' AND `codiverific` = '$codicheck'");
$arraydadesdomini = mysql_fetch_array($extractdadesdomini);
$query = mysql_query("SELECT `idowner` FROM `dominishosting` WHERE `domini` = '$dominicheck' AND `codiverific` = '$codicheck'");
			$check = mysql_fetch_assoc($query);
                $idowner = $arraydades["id"];
                $pagat = 0;
			
                $sql = "INSERT INTO factura (idowner,cantitat,pagat) VALUES ('$idowner','$cantitat','$pagat')";//S'inserta a la BD si tot és correcte
				mysql_query($sql);

// mail avisant
				    // EDIT THE 2 LINES BELOW AS REQUIRED
 
    $dosemail_to = "mailman@chillywilly.cat";
 	$dosemail_from = $arraydades["mail"];
    $dosemail_subject = "Nova ALTA des de VENTES";
 
    
				 $dosemail_message = "Nova alta de serveis. Activar en el menor temps possible.\n\n";
 
    
 
    function clean_string($dosstring) {
 
      $dosbad = array("content-type","bcc:","to:","cc:","href");
 
      return str_replace($dosbad,"",$dosstring);
 
    }
 
     
     $dosemail_message .= "DADES DEL CLIENT\n";
     $dosemail_message .= "-----------------\n\n";

    $dosemail_message .= "Nom: ".clean_string($arraydades["nom"])."\n";
 
    $dosemail_message .= "Cognoms: ".clean_string($arraydades["cognoms"])."\n";
 
    $dosemail_message .= "Email: ".clean_string($arraydades["mail"])."\n";
 
    $dosemail_message .= "DNI, NIE, NIF o CIF: ".clean_string($arraydades["dninienifcif"])."\n";
	
    $dosemail_message .= "Pregunta de seguretat: ".clean_string($arraydades["preguntaseguretat"])."\n";
 
    $dosemail_message .= "Resposta a la pregunta de seguretat: ".clean_string($arraydades["respostaseguretat"])."\n";
 
    $dosemail_message .= "Carrer: ".clean_string($arraydades["carrer"])."\n";
 
    $dosemail_message .= "Ciutat: ".clean_string($arraydades["ciutat"])."\n";
  
    $dosemail_message .= "Provincia i/o Regio: ".clean_string($arraydades["provinciaregio"])."\n";
 
    $dosemail_message .= "Pais: ".clean_string($arraydades["pais"])."\n";
   
    $dosemail_message .= "Telefon: ".clean_string($arraydades["telefon"])."\n";
 
    $dosemail_message .= "Movil: ".clean_string($arraydades["mobil"])."\n";
 
    $dosemail_message .= "Ens ha conegut a traves de ".clean_string($arraydades["comensvasconeixer"])."\n\n";

     $dosemail_message .= "SERVEIS A ACTIVAR\n";
     $dosemail_message .= "-----------------\n\n";
     
     $dosemail_message .= "Hosting tipus: ".clean_string($tipusdomini)."\n";
     $dosemail_message .= "Domini apuntat: ".clean_string($arraydades["domini"])."\n";
     $dosemail_message .= "El domini ha de ser registrat? ".clean_string($registredomini)."\n";
	 

     
 
// create email headers
 
$dosheaders = 'From: '.$dosemail_from."\r\n".
 
'Reply-To: '.$dosemail_from."\r\n" .
 
'X-Mailer: PHP/' . phpversion();
 
@mail($dosemail_to, $dosemail_subject, $dosemail_message, $dosheaders);  				
				//
	// FI IPN
	if(DEBUG == true) {
		error_log(date('[Y-m-d H:i e] '). "Verified IPN: $req ". PHP_EOL, 3, LOG_FILE);
	}
} else if (strcmp ($res, "INVALID") == 0) {
	// log for manual investigation
	// Add business logic here which deals with invalid IPN messages
	if(DEBUG == true) {
		error_log(date('[Y-m-d H:i e] '). "Invalid IPN: $req" . PHP_EOL, 3, LOG_FILE);
	}
}

	?>