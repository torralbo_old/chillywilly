<?php if (!isset($_COOKIE['lenguaje'])){
     $sitelangA = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if (preg_match('/es/i',$sitelangA)) {
setcookie('lenguaje', "es");
echo '<meta http-equiv="refresh" content="0">';
} 
elseif (preg_match('/ca/i',$sitelangA)) 
{
setcookie('lenguaje', "cat");
echo '<meta http-equiv="refresh" content="0">';
}
elseif (preg_match('/en/i',$sitelangA)) {
setcookie('lenguaje', "en");
echo '<meta http-equiv="refresh" content="0">';
}
}

if ($_COOKIE['lenguaje'] == "es") {
	include '../php/languages/es.php';
} elseif ($_COOKIE['lenguaje'] == "cat") {
	include '../php/languages/cat.php';
} elseif ($_COOKIE['lenguaje'] == "en") {
	include '../php/languages/en.php';
}	
  ?>
<!DOCTYPE html>
<html lang="es-ES">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Chillywilly - Allotjament web de qualitat</title>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link href="../css/bootstrap.min.css" rel="stylesheet">
<link href="../style.css" rel="stylesheet">
<link href="../css/progress.css" rel="stylesheet">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]--> </head>
<body>
<div class="wrapper purple-content">
<div class="container">
<div class="content-heading white">
<h2><?php echo $n220 ?></h2>
<small><?php echo $n221 ?></small>
</div>
</div>
</div><div class="wrapper" id="maincontent">
<div class="container">
<div class="dropdown-box active" id="contactbox">

<div class="content-heading">

<h2><?php echo $n222 ?></h2><br><br>
<small><?php echo $n223 ?></small>
</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<script src="../js/isotope.js"></script>
<script src="../js/progress.js"></script>
<script src="../js/jquery-scrollto.js"></script>
<script src="../custom.js"></script> </body>
</html>