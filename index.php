﻿<?php include './php/header/variables.php';
if (!isset($_COOKIE['lenguaje'])){
     $sitelangA = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if (preg_match('/es/i',$sitelangA)) {
include './php/languages/es.php';
setcookie('lenguaje', "es");
} 
elseif (preg_match('/ca/i',$sitelangA)) 
{
include './php/languages/cat.php';
setcookie('lenguaje', "cat");
}
elseif (preg_match('/en/i',$sitelangA)) {
include './php/languages/en.php';
setcookie('lenguaje', "en");
}
}

if ($_COOKIE['lenguaje'] == "es") {
	include './php/languages/es.php';
} elseif ($_COOKIE['lenguaje'] == "cat") {
	include './php/languages/cat.php';
} elseif ($_COOKIE['lenguaje'] == "en") {
	include './php/languages/en.php';
}	
?>
<!DOCTYPE html>
<html lang="es-ES">
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $n20; ?> - <?php echo $n21; ?></title>
	<?php echo $csslinks; ?>
  </head>
<?php include './php/header/headermenu.php'; ?>
<?php echo cookieslaw(); ?>
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="100000">
            <div id="plancarousel" class="carousel-inner" role="listbox">
                     
   	
                
                <div class="row-fluid item active">
                    <div class="col-md-3 col-sm-6">
                        <div class="plan notfeatured">
                        	<span class="price">6<?php echo $n22; ?></span>
                            <h3><?php echo $n5; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                                <li><span class="fa fa-download"></span>5 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span>5 <?php echo $n27; ?></li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span>2 <?php echo $n28; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span>1 <?php echo $n29; ?></li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="hosting"><?php echo $n133; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan red featured">
                        	<span class="price">10<?php echo $n22; ?></span>
                            <h3><?php echo $n6; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                                <li><span class="fa fa-download"></span>10 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span>15 <?php echo $n27; ?></li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span>8 <?php echo $n28; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span>3 <?php echo $n29; ?></li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="hosting"><?php echo $n133; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan purple notfeatured">
                        	<span class="price">15<?php echo $n22; ?></span>
                            <h3><?php echo $n7; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                                <li><span class="fa fa-download"></span>20 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span><?php echo $n27; ?> <?php echo $n241; ?></li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span>15 <?php echo $n28; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span>6 <?php echo $n29; ?></li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="hosting"><?php echo $n133; ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="plan green notfeatured">
                        	<span class="price">20<?php echo $n22; ?></span>
                            <h3><?php echo $n8; ?></h3>
                            <small><?php echo $n32; ?> <?php echo datasismesos(); ?></small>
                            <ul>
                               <li><span class="fa fa-download"></span>30 GB <?php echo $n23; ?></li>
                                <li><span class="fa fa-share-square"></span><?php echo $n26; ?></li>
                                <li><span class="fa fa-globe"></span><?php echo $n27; ?> <?php echo $n241; ?></li>
                                <li class="hidden-sm"><span class="fa fa-envelope"></span><?php echo $n99; ?></li>
                                <li class="hidden-sm"><span class="fa fa-database"></span><?php echo $n29; ?> <?php echo $n242; ?></li>
                                <li class="hidden-md"><span class="fa fa-ticket"></span><?php echo $n33; ?></li>
                                <li><a href="hosting"><?php echo $n133; ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            
            </div>
            

            </div>
        </div>
    </div>
      <div class="wrapper blue-content" id="domainmain">
    	<div class="container">
        	<div class="content-heading white">
    			<h2 id="domainscroll"><?php echo $n39; ?></h2>
                <small><?php echo $n40; ?></small>
                <span></span>
            </div>
                <!-- per eliminar despres -->
<script type="text/javascript" src="/ver_dominio/jquery-1.2.6.min.js"></script>

<script language="javascript">
$(document).ready(function() {
	
	var loading;
	var results;
	var form;
	
	form = document.getElementById('form');
	loading = document.getElementById('loading');
	results = document.getElementById('results');
	
	$('#Submit').click( function() {
		
		if($('#Search').val() == "")
		{alert('404 NOT FOUND');return false;}
		
		results.style.display = 'none';
		$('#results').html('');
		loading.style.display = 'inline';
		
		$.post('/ver_dominio/process.php?domain=' + escape($('#Search').val()),{
		}, function(response){
			
			results.style.display = 'block';
			$('#results').html(unescape(response));	
			loading.style.display = 'none';
		});
		
		return false;
	});
	
});
</script>

	
	

	
    <!-- fi elim -->
            <div class="search-wiki">	<form method="post" action="./" id="form"> 

            	<input type="text" value="" placeholder="<?php echo $n41; ?>"  autocomplete="off" id="Search" name="domain"><input id="Submit" type="submit" value="<?php echo $n42; ?>">	</form>

            </div><div id="loading"></div>
		<br><br>
	 <div id="results" align="center">
		
	 </div>	
		</div>
        <svg id="cloudstop" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
    <path d="M-5 100 Q 0 20 5 100 Z
         M0 100 Q 5 0 10 100
         M5 100 Q 10 30 15 100
         M10 100 Q 15 10 20 100
         M15 100 Q 20 30 25 100
         M20 100 Q 25 -10 30 100
         M25 100 Q 30 10 35 100
         M30 100 Q 35 30 40 100
         M35 100 Q 40 10 45 100
         M40 100 Q 45 50 50 100
         M45 100 Q 50 20 55 100
         M50 100 Q 55 40 60 100
         M55 100 Q 60 60 65 100
         M60 100 Q 65 50 70 100
         M65 100 Q 70 20 75 100
         M70 100 Q 75 45 80 100
         M75 100 Q 80 30 85 100
         M80 100 Q 85 20 90 100
         M85 100 Q 90 50 95 100
         M90 100 Q 95 25 100 100
         M95 100 Q 100 15 105 100 Z"></path>
</svg>
    </div>
    <div class="wrapper" id="maincontent">
    	<div class="container">
        	<div class="content-heading">
    			<h2><?php echo $n50; ?></h2>
                <small><?php echo $n51; ?></small>
                <span></span>
            </div>
            <div class="row mainfeatures">
            	<div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-briefcase"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n52; ?></h3>
                            <p><?php echo $n53; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-lock"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n54; ?></h3>
                            <p><?php echo $n55; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-cog"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n56; ?></h3>
                            <p><?php echo $n57; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-file"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n58; ?></h3>
                            <p><?php echo $n59; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-headphones"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n60; ?></h3>
                            <p><?php echo $n61; ?></p>
                         </div>
                    </div>
                </div>
                <div class="col-sm-6">
                	<div class="row">
                        <div class="col-sm-2">
                            <span class="glyphicon glyphicon-euro"></span>
                        </div>
                        <div class="col-sm-10">
                            <h3><?php echo $n62; ?></h3>
                            <p><?php echo $n63; ?></p>
                         </div>
                    </div>
                </div>
            </div>
            
        </div>
        
        <svg id="cloudsbottom" xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100" viewBox="0 0 100 100" preserveAspectRatio="none">
    <path d="M-5 100 Q 0 20 5 100 Z
         M0 100 Q 5 0 10 100
         M5 100 Q 10 30 15 100
         M10 100 Q 15 10 20 100
         M15 100 Q 20 30 25 100
         M20 100 Q 25 -10 30 100
         M25 100 Q 30 10 35 100
         M30 100 Q 35 30 40 100
         M35 100 Q 40 10 45 100
         M40 100 Q 45 50 50 100
         M45 100 Q 50 20 55 100
         M50 100 Q 55 40 60 100
         M55 100 Q 60 60 65 100
         M60 100 Q 65 50 70 100
         M65 100 Q 70 20 75 100
         M70 100 Q 75 45 80 100
         M75 100 Q 80 30 85 100
         M80 100 Q 85 20 90 100
         M85 100 Q 90 50 95 100
         M90 100 Q 95 25 100 100
         M95 100 Q 100 15 105 100 Z"></path>
</svg>
    </div>
    
    <div class="wrapper" id="darkerfooter">
<?php include './php/footer/footer.php' ; ?>

<?php echo $jslinks; ?>
  </body>
</html>