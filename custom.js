jQuery(document).ready(function() {
    "use strict";
    $(".progress").each(function () {
        var progressNumb = $(this).data("progress");
        $(this).rounders({
            percent: progressNumb
        });
    });
    $('.dropdown-toggle').dropdown();
    $('#topfeatures [class*="col"]').popover({
        trigger: "hover"
    });
    $('.help-toggle').popover({
        trigger: "hover"
    });
    $('html').on('click', function (event) {
        var target = $(event.target);
        if ($(target).parents('#domainbulk').length > 0 || $(target).parents('#domainprices').length > 0 || $(target).parents('.hiddenbox').length > 0 || target.is(".hiddenbox") || $(target).parents('#header ul').length > 0) {} else {
            $(".visible").removeClass('visible');
        }


    });
    $('.logo a').on('click', function (e) {
        e.preventDefault();
        jQuery("#topbar").slideToggle(200);
    });
    $('#faqbox h3 a').on('click', function (e) {
        e.preventDefault();
        event.stopPropagation();
        $(this).parent().parent().find(".answer").toggleClass("active");
    });
    $('.scrolltop').on('click', function (e) {
        e.preventDefault();
        jQuery("#topbar").ScrollTo();
    });
    $('.dropdown-toggle').on('click', function (e) {
        e.preventDefault();
        event.stopPropagation();
        var boxid = $(this).data("toggle");
        if ($(this).parents('#domainmain').length > 0) {
            jQuery("#domainscroll").ScrollTo();
        }
        if ($(this).hasClass("visible")) {
            $(".visible").removeClass('visible');
        } else {
            $(".visible").removeClass('visible');
            $(this).addClass('visible');
            $("#" + boxid).addClass("visible");
        }
    });
    $('.hosting-toggle').on('click', function (e) {
        e.preventDefault();
        $(this).parent().parent().find(".active").removeClass("active");
        $(this).parent().addClass("active");
        var boxID = $(this).data("toggle");
        $('.dropdown-box.active').removeClass("active");
        $('#' + boxID).addClass("active");
    });
    $('#header > ul li.dropdown-stop > a').on('click', function (event) {
        event.preventDefault();
        $(this).parent().toggleClass("hovered");
        $(this).parent().children("ul").toggleClass("visible");
    });
    $('.plan').on('mouseover', function (event) {
        $(this).parent().parent().find(".featured").removeClass("featured").addClass("notfeatured");
        $(this).removeClass("notfeatured").addClass("featured");
    });
    $('.hosting-types .col-sm-4').on('mouseover', function (event) {
        $(this).parent().parent().find(".featured").removeClass("featured").addClass("notfeatured");
        $(this).removeClass("notfeatured").addClass("featured");
    });
    $('.compareplans ul li ul li:not(:first-child)').on('mouseover', function (event) {
        var indexComp = ($(this).index()) + 1;
        $(".compareplans ul li ul li.featured").removeClass("featured");
        $(".compareplans ul li ul").each(function (index) {
            $(".compareplans ul li ul li:nth-child(" + indexComp + ")").addClass("featured");
        });
    });
    $('.plan').on('click', function (event) {
        $(this).parent().parent().find(".featured").removeClass("featured").addClass("notfeatured");
        $(this).removeClass("notfeatured").addClass("featured");
    });
    $('.phonemenu').on('click', function (event) {
        $(this).parent().find("ul").toggleClass("shown");
    });
    $('.extcheck').on('click', function () {
        if ($(this).find('input[type="radio"]').attr("value") === "off") {
            $(".extensionslist").removeClass("shown");
        } else {
            $(".extensionslist").addClass("shown");
        }
    });
    $(window).load(function () {
        var $container = $('#clientwebsites').isotope({
            filter: "*"
        }), $container2 = $('#testimonialgrid').isotope({}), $container3 = $('#bloggrid').isotope({});
        $('#clientwebsitesfilter li a').on('click', function (e) {
            var textAlt = $(this).text(), filterValue = $(this).attr('data-filter');
            $(this).parent().parent().parent().find("button").html(textAlt + ' <span class="caret"></span>');
            e.preventDefault();
            filterValue = $(this).attr('data-filter');
            $container.isotope({
                filter: filterValue
            });
        });

        $('#testimonialfilter li a').on('click', function (e) {
            var textAlt2 = $(this).text(), filterValue2 = $(this).attr('data-filter');
            $(this).parent().parent().parent().find("button").html(textAlt2 + ' <span class="caret"></span>');
            e.preventDefault();
            $container2.isotope({
                filter: filterValue2
            });
        });

        $('#blogfilter li a').on('click', function (e) {
            var textAlt3 = $(this).text(), filterValue3 = $(this).attr('data-filter');
            $(this).parent().parent().parent().find("button").html(textAlt3 + ' <span class="caret"></span>');
            e.preventDefault();
            $container3.isotope({
                filter: filterValue3
            });
        });
    });
});